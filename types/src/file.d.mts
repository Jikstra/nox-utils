/**
 * @param {string} file
 */
export function _includeFile(file: string, increase_pos_on_stack?: number): {
    full_path: string;
    callers_path: string;
    contents: string;
};
/**
 * @param {string} file
 */
export function includeFile(file: string, increase_pos_on_stack?: number): string;
/**
 * @param {ImportMeta} importMeta
 */
export function ___filename(importMeta: ImportMeta): string;
/**
 * @param {ImportMeta} importMeta
 */
export function ___dirname(importMeta: ImportMeta): string;
/**
 * @param {string} str
 */
export function removeLineBreaks(str: string): string;
