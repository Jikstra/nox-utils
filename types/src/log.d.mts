/**
 * @param {LOG_LEVEL} level
 */
export function setLogLevel(level: LOG_LEVEL): void;
/**
 * @param {string} tty_color
 * @param {string} prefix
 * @param {any[]} args
 */
export function _log(tty_color: string, prefix: string, ...args: any[]): void;
/**
 * @param {any[]} args
 */
export function logDebug(...args: any[]): void;
/**
 * @param {any[]} args
 */
export function logInfo(...args: any[]): void;
/**
 * @param {any[]} args
 */
export function logOk(...args: any[]): void;
export function registerSSGErrorHandler(): void;
export const TTYReset: "\u001B[0m";
export const TTYBright: "\u001B[1m";
export const TTYDim: "\u001B[2m";
export const TTYUnderscore: "\u001B[4m";
export const TTYBlink: "\u001B[5m";
export const TTYReverse: "\u001B[7m";
export const TTYHidden: "\u001B[8m";
export const TTYFgBlack: "\u001B[30m";
export const TTYFgRed: "\u001B[31m";
export const TTYFgGreen: "\u001B[32m";
export const TTYFgYellow: "\u001B[33m";
export const TTYFgBlue: "\u001B[34m";
export const TTYFgMagenta: "\u001B[35m";
export const TTYFgCyan: "\u001B[36m";
export const TTYFgWhite: "\u001B[37m";
export const TTYBgBlack: "\u001B[40m";
export const TTYBgRed: "\u001B[41m";
export const TTYBgGreen: "\u001B[42m";
export const TTYBgYellow: "\u001B[43m";
export const TTYBgBlue: "\u001B[44m";
export const TTYBgMagenta: "\u001B[45m";
export const TTYBgCyan: "\u001B[46m";
export const TTYBgWhite: "\u001B[47m";
export const TTYSpacer: "    ";
export type LOG_LEVEL = number;
export namespace LOG_LEVEL {
    const OK: number;
    const INFO: number;
    const WARN: number;
    const DEBUG: number;
}
export class NoStackError extends Error {
    /**
     * @param {any} error
     */
    constructor(error: any);
    error: any;
}
