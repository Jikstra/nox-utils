/**
 * @param {string} file
 */
export function includeLess(file: string): string;
/**
 * @param {TemplateStringsArray} strings
 * @param {any[]} values
 */
export function less(strings: TemplateStringsArray, ...values: any[]): string;
export namespace LESS_OPTIONS {
    const syncImport: boolean;
    const sync: boolean;
}
export class BeautifiedLessError extends Error {
    /**
     * @param {string} file
     * @param {Less.RenderError} raw_less_error
     */
    constructor(file: string, raw_less_error: Less.RenderError);
}
