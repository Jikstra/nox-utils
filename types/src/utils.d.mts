/**
 * @param {TemplateStringsArray} strings
 * @param {any[]} values
 */
export function css(strings: TemplateStringsArray, ...values: any[]): string;
