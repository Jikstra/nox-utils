/**
 * @typedef {object} ReflectObject
 * @property {string|null} file
 * @property {number} lineNumber
 * @property {number} position
 * @property {string|null} method
 * @property {string|null} raw
 */
/**
 * @param {string} stack_line
 * @returns {ReflectObject|null}
 */
export function parseStackTraceLine(stack_line: string): ReflectObject | null;
export function reflect(stack_pos?: number): ReflectObject | null;
export function callersPath(stack_offset?: number): string;
export const REFLECT_DEFAULT_STACK_POS: 2;
export type ReflectObject = {
    file: string | null;
    lineNumber: number;
    position: number;
    method: string | null;
    raw: string | null;
};
