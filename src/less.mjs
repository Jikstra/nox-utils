import lessNode from 'less'
// @ts-ignore
import LessError from 'less/lib/less/less-error.js'

import path from 'node:path'
import {removeLineBreaks, _includeFile} from './file.mjs'
import {reflect} from './reflect.mjs'
import {TTYSpacer} from './log.mjs'

export const LESS_OPTIONS = {
  syncImport: true,
  sync: true,
}

/**
 * @param {string} file
 */
export function includeLess(file) {
  const { contents, full_path } = _includeFile(file, 2) 
  if (contents == null) {
    throw new Error('includeLess: Cannot open file')
  }
  
  const paths = [path.dirname(full_path)]
  const lessOptions = {
    ...LESS_OPTIONS,
    paths
  }

  let renderedString = ''
  try {

    /**
     * @param {Less.RenderError} err
     * @param {Less.RenderOutput | undefined} result
     */
    function cb(err, result) {
      if (err) throw new BeautifiedLessError(full_path, err)
      if (result?.css) renderedString = result.css
    }
    lessNode.render(contents, lessOptions, cb)
  } catch (/** @type {BeautifiedLessError|LessError|unknown} */err) {

    if (err instanceof LessError) {
      throw new BeautifiedLessError(full_path, err)
    }
    throw err
  }
  
  return removeLineBreaks(renderedString)
}

/**
 * @param {TemplateStringsArray} strings
 * @param {any[]} values
 */
export function less(strings, ...values) {
  let concatenatedString = ''
  for (let i = 0; i < strings.length; i++) {
    const string = strings[i];
    for (let j = 0; j < string.length; j++) {
      concatenatedString += string[j]
    }
    if (i < values.length) {
      concatenatedString += values[i]
    }
  }



  let renderedString = "" 
  lessNode.render(concatenatedString, LESS_OPTIONS, (err, result) => {
    if (err) {
      const reflect_object = reflect(2)

      throw new BeautifiedLessError(reflect_object?.file || '??', err)
    }
    renderedString = result?.css || ''
  })

  return removeLineBreaks(renderedString)
}

export class BeautifiedLessError extends Error {
  /**
   * @param {string} file
   * @param {Less.RenderError} raw_less_error
   */
  constructor(file, raw_less_error) {
    let message = ''
    message += `\r\n${TTYSpacer}Error in less file ` + file + ':' + raw_less_error.line
    if (raw_less_error.extract[0]) {
      message += `\r\n${TTYSpacer}${TTYSpacer}` + (raw_less_error.line - 1) + ' |    ' + raw_less_error.extract[0] 
    }
    message += `\r\n${TTYSpacer}${TTYSpacer}` + raw_less_error.line + ' |    ' + raw_less_error.extract[1] 
    message += `\r\n${TTYSpacer}${TTYSpacer}` + ' '.repeat(raw_less_error.line.toString().length) + '      ' + ' '.repeat(raw_less_error.column) + '^^^^'
    message += `\r\n${TTYSpacer}${TTYSpacer}` + ' '.repeat(raw_less_error.line.toString().length) + '      ' + ' '.repeat(raw_less_error.column) + raw_less_error.message
    if (raw_less_error.extract[2]) {
      message += `\r\n${TTYSpacer}${TTYSpacer}` + (raw_less_error.line - 1) + ' |    ' + raw_less_error.extract[2] 
    }

    super(message)
  }
}
