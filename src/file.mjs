import {readFileSync} from "node:fs"
import path from "node:path"
import {fileURLToPath} from "node:url"
import {callersPath} from "./reflect.mjs"

/**
 * @param {string} file
 */
export function _includeFile(file, increase_pos_on_stack=1) {
  const callers_path = callersPath(increase_pos_on_stack) 
  const full_path = path.join(callers_path, file)
  const contents = readFileSync(full_path).toString()
  return { full_path, callers_path, contents }
}

/**
 * @param {string} file
 */
export function includeFile(file, increase_pos_on_stack=1) {
  return _includeFile(file, increase_pos_on_stack).contents
}



/**
 * @param {ImportMeta} importMeta
 */
export function ___filename(importMeta) {
  return fileURLToPath(importMeta.url)
}

/**
 * @param {ImportMeta} importMeta
 */
export function ___dirname(importMeta) {
  return path.dirname(___filename(importMeta))
}

/**
 * @param {string} str
 */
export function removeLineBreaks(str) {
  // Remove line breaks
  str = str.replace(/(\r\n|\n|\r)/gm, "")
  // Remove multiple white spaces
  str = str.replace(/ +(?= )/gm, "")
  // Trim
  str = str.trim()
  return str
}
