/*
 * # Log & Error System
 *
 * Implement Error Types and a nodejs global error handler that better suits our needs for a Static Site Generator.
 */

export const TTYReset = "\x1b[0m"
export const TTYBright = "\x1b[1m"
export const TTYDim = "\x1b[2m"
export const TTYUnderscore = "\x1b[4m"
export const TTYBlink = "\x1b[5m"
export const TTYReverse = "\x1b[7m"
export const TTYHidden = "\x1b[8m"

export const TTYFgBlack = "\x1b[30m"
export const TTYFgRed = "\x1b[31m"
export const TTYFgGreen = "\x1b[32m"
export const TTYFgYellow = "\x1b[33m"
export const TTYFgBlue = "\x1b[34m"
export const TTYFgMagenta = "\x1b[35m"
export const TTYFgCyan = "\x1b[36m"
export const TTYFgWhite = "\x1b[37m"

export const TTYBgBlack = "\x1b[40m"
export const TTYBgRed = "\x1b[41m"
export const TTYBgGreen = "\x1b[42m"
export const TTYBgYellow = "\x1b[43m"
export const TTYBgBlue = "\x1b[44m"
export const TTYBgMagenta = "\x1b[45m"
export const TTYBgCyan = "\x1b[46m"
export const TTYBgWhite = "\x1b[47m"

export const TTYSpacer = '    '



/**
 * @readonly
 * @enum {number}
 */
export const LOG_LEVEL = {
  OK: 0,
  INFO: 1,
  WARN: 2,
  DEBUG: 3,
}

/* @type {LOG_LEVEL} log_level */
let log_level = LOG_LEVEL.DEBUG


/**
 * @param {LOG_LEVEL} level
 */
export function setLogLevel(level) {
  log_level = level
}

/**
 * @param {string} tty_color
 * @param {string} prefix
 * @param {any[]} args
 */
export function _log(tty_color, prefix, ...args) {
  console.log(`${tty_color}\[${prefix}\]`, ...args, TTYReset)
}
/**
 * @param {any[]} args
 */
export function logDebug(...args) {
  if (log_level < LOG_LEVEL.DEBUG) return
  _log(TTYFgBlue, 'Debug', ...args)
}

/**
 * @param {any[]} args
 */
export function logInfo(...args) {
  _log(TTYFgYellow, 'Info', ...args)
}
/**
 * @param {any[]} args
 */
export function logOk(...args) {
  _log(TTYFgGreen, 'Ok', ...args)
}
export class NoStackError extends Error {
  /**
   * @param {any} error
   */
  constructor(error) {
    super();
    this.error = error
  }
}

export function registerSSGErrorHandler() {
  process.on('unhandledRejection', error => {
    console.log(TTYFgRed, error, TTYReset)
    process.exit(1)
  });
}

