import {removeLineBreaks} from './file.mjs';

/**
 * @param {TemplateStringsArray} strings
 * @param {any[]} values
 */
export function css(strings, ...values) {
  let concatenatedString = ''
  for (let i = 0; i < strings.length; i++) {
    const string = strings[i];
    for (let j = 0; j < string.length; j++) {
      concatenatedString += string[j]
    }
    if (i < values.length) {
      concatenatedString += values[i]
    }
  }
  return removeLineBreaks(concatenatedString)
}


