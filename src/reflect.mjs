/*
 * # Reflection
 *
 * This abuses throwing and catching an Error to retrieve our call stack. This we use to estimate
 * from which function we got called and determine this parent's file and function.
 *
 * This is commonly used to figure out our filepath in es6 and to have relative includes.
 * Could also be used for custom error outputs.
 */

import path from "node:path"

 

/**
 * @typedef {object} ReflectObject
 * @property {string|null} file
 * @property {number} lineNumber
 * @property {number} position
 * @property {string|null} method
 * @property {string|null} raw
 */
 

// Helper function to parse a strack trace line that we received as a string from any catched Error.
/**
 * @param {string} stack_line
 * @returns {ReflectObject|null}
 */
export function parseStackTraceLine(stack_line) {
  stack_line = stack_line.replace(" at", "").trim()

  /** @type {ReflectObject} reflect_object */
  const reflect_object = {
    file: null,
    lineNumber: -1,
    position: -1,
    method: null,
    raw: stack_line
  }

  const regexp_method_name_file_line_pos = new RegExp('(.*) \\((.*):(.*):(.*)\\)', 'gm')
  const regexp_file_line_pos = new RegExp('(.*):(.*):(.*)', 'gm')


  const matches_method_name_file_line_pos = regexp_method_name_file_line_pos.exec(stack_line)
  if (matches_method_name_file_line_pos !== null) {
    reflect_object.method = matches_method_name_file_line_pos[1] 
    reflect_object.file = matches_method_name_file_line_pos[2]
    reflect_object.lineNumber = Number(matches_method_name_file_line_pos[3])
    reflect_object.position = Number(matches_method_name_file_line_pos[4])
  } else {
    const matches_file_line_pos = regexp_file_line_pos.exec(stack_line)
    if (matches_file_line_pos !== null) {
      reflect_object.file = matches_file_line_pos[1]
      reflect_object.lineNumber = Number(matches_file_line_pos[2])
      reflect_object.position = Number(matches_file_line_pos[3])
    }
  }

  if (reflect_object.file && reflect_object.file.startsWith('file://')) {
    reflect_object.file = reflect_object.file.replace('file://', '')
  }

  return reflect_object
}


export const REFLECT_DEFAULT_STACK_POS = 2
export function reflect(stack_pos=REFLECT_DEFAULT_STACK_POS) {
  const err = new Error();
  if (!err?.stack) return null
  let stack_lines = err.stack.split("\n")

  // remove first element of array
  stack_lines.shift()

  if (stack_pos > (stack_lines.length - 1)) {
    return null
  }
  const reflect_object = parseStackTraceLine(stack_lines[stack_pos])
  return reflect_object
}

export function callersPath(stack_offset=0) {
  const reflect_object = reflect(2+stack_offset)
  if (!reflect_object?.file) {
    throw new Error('ReflectObject is null, wrong stack_offset?')
  }
  return path.dirname(reflect_object.file)
}
